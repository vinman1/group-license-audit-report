# Group license report

Download all license reports of all projects in a group and produce one CSV report

## Retrieving license scanning artifacts

Please note that the native GitLab CI template for license scanning does not publish the license scanning report as an artifact. Thus it can't be retrieved from the job artifact API.

In order for this script to work, license scanning must be activated in projects by `including` [a modified CI file](license-scanning-with-artifact.yml) that adds the license scanning report file to `artifacts:paths`.

So instead of

```yml
include:
  - template: Security/License-Scanning.gitlab-ci.yml
```

you would need to

```yml
include:
  - remote: 'https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/group-license-audit-report/-/raw/master/license-scanning-with-artifact.yml'
```

Or alternatively host this modified template together with your CI template library in your own group or instance.

## Features

- Queries all projects in a group or alternatively, all specified projects in config.yml
- Finds the latest successful master pipeline
- Identifies `license` (scanning / management) jobs in that pipeline and retrieves the license report json artifact
- Provides a combined json of all license scanning reports as well as a csv representation
- Leverages junit XML output to log projects without license scanning

## Usage

`python3 license-audit-report.py $GIT_TOKEN config.yml`

## Configuration

- Export / fork repository.
- edit config.yml
  - specify GitLab URL and groups or projects using their ID.
  - If groups are used, all projects in the group are retrieved instead of individual projects.
- Create a CI/CD variable "GIT_TOKEN" with an API token that has access to the group you want to run it on
- Run the Pipeline to get your report
- Navigate to pipeline artifacts to get your report
- Navigate to pipeline tests to see if it was complete or identify any errors
